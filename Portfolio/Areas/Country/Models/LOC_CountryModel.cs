﻿using System.ComponentModel.DataAnnotations;

namespace Portfolio.Areas.Country.Models
{
    public class LOC_CountryModel
    {
        public int ContryID { get; set; }

        [Required(ErrorMessage = "Enter Appropriate Country Name")]
        public string CountryName { get; set; }

        [Required(ErrorMessage = "Enter Appropriate Country Code")]
        public string CountryCode { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }
    }
}
